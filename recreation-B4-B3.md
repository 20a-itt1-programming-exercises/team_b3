Recreation checklist for team <other team's name>
===================================================

Team name: <your teamname>

Checklist
------------

- [x] Project plan completed
- [ ] System block diagram completed
- [ ] Recreate documentation clear and understandable
- [ ] ADC communicating with RPi
- [ ] Temperature sensor communicating with RPi
- [ ] BTN/LED board communicating with RPi
- [ ] Data from RPi sent to Thingspeak


Comments
-----------

<add a bulleted list of bug issues you have created on the projects of other team's>

<Any comments that may be relevant for you, other team's or teachers. (Could be that something is super successfull)>