Recreation checklist for team <A4>
===================================================

Team name: <B3>

Checklist
------------

- [x] Project plan completed
- [ ] System block diagram completed
- [x] Recreate documentation clear and understandable
- [ ] ADC communicating with RPi
- [ ] Temperature sensor communicating with RPi
- [ ] BTN/LED board communicating with RPi
- [ ] Data from RPi sent to Thingspeak


Comments
-----------

<add a bulleted list of bug issues you have created on the projects of other team's>

<Any comments that may be relevant for you, other team's or teachers. (Could be that something is super successfull)>