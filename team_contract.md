B3 Team contract

-	Review contract in two weeks
-	Be nice and respect each other 
-	Notify the team in due time if you are delayed or  absent
-	Be willing to share your knowledge 
-	A shared calendar (school related)
-	An agenda for every meeting , posted the day before
-	Systemized assigned tasks
-   Agreed timeframe before meetings

Presentation 

One strength from each

Camilla    - Like to make stuff happen
Henrik     - Prior Experience in programming
Gladys     - Persistent and curious
Aubrey     - Keep the conversation going
Aleksandra - Strong logical thinking
Lukas      - Helpful
Carsten    - Work best under pressure
Tobias     - Work hard to get things done

Shared interest of the team
Boardgames, Beer, Rock music, New cultures and languages, Travel, Games

Goals for teamwork
	To create a safe and inspiring environment for learning
	To evolve individually and as a team
	To learn about IT technology 
	
Frequency of meetings 
	After School once a week for about 1-2 hour duration - adapted as needed

Communication
	Discord, E-mail, Gitlab, Text, Messenger, Shared google calendar
	
File sharing
	Gitlab, google drive - worst case scenario: email

3 Most important from the contract
	1)Be nice and respect each other 
	2)Be willing to share your knowledge
	3)Planning and notifying 
	
Why the team is awesome
